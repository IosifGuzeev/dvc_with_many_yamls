import sys
import pandas as pd
from pathlib import Path


if __name__ == "__main__":
    input = Path(sys.argv[1])
    output = Path(sys.argv[2])

    output.parent.mkdir(exist_ok=True, parents=True)

    data = pd.read_csv(input)
    statistic = data.describe()
    statistic.to_csv(output, index=False)
