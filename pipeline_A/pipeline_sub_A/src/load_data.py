import sys
import numpy as np
import pandas as pd
from pathlib import Path


if __name__ == "__main__":
    output = Path(sys.argv[1])

    output.parent.mkdir(exist_ok=True, parents=True)

    data = np.random.uniform(0, 100, (1000, 2))
    data = pd.DataFrame(data=data, columns=["X", "Y"])
    data.to_csv(output, index=False)
